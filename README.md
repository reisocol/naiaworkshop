# Naiaworkshop



## Getting started

The Office project is aimed to programmers interested in creating gameplay framework classes in first person context game to practice the first concepts of the unreal engine framework.

This is part of the Naia initiative program from Sidia.

You can fork this repository to be working freely at your own pace.

Do not forget to watch the workshop video to follow the instructions in game engine setup and programming tips.

![Alt text](https://drive.google.com/uc?id=1P8Q5ObFSNtKh8dOFJ1x5FM1KsrGkiuUq&export=download)


## Branches

You can work freely on the main branch developing your own solution or in case you have any doubts or wanna check some implementation just checkout the feature/mechanics branch.

## Feedback

If you have some questions about the projects or anything you want to discuss, do not hesitate to contact me at rodrigo.reis@sidia.com

Enjoy.